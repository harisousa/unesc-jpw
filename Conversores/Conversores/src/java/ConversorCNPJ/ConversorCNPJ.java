package ConversorCNPJ;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter(forClass = CnpjMB.class)
public class ConversorCNPJ implements Converter {

    @Override
    // Método para converter o CNPJ formatado (11.222.333/444-55) para não formatado (1122233344455)
    public Object getAsObject(FacesContext context, UIComponent component, String value) throws ConverterException {
        String cnpj = value;
        if (value != null && !value.equals("")) {
            cnpj = value.replaceAll("\\.", "").replaceAll("\\-", "").replaceAll("\\/","");                                                                          
        }
        return cnpj;
    }

    @Override
    // Método para converter o CNPJ não formatado (1122233344455) para formatado (11.222.333/444-55)
    public String getAsString(FacesContext context, UIComponent component, Object value) throws ConverterException {
        String cnpj = value.toString();
        if (cnpj != null && cnpj.length() == 14) {
            cnpj = cnpj.substring(0, 2) + "." + cnpj.substring(2, 5) + "." + cnpj.substring(5, 8) + "/" + cnpj.substring(8, 12) + "-" + cnpj.substring(12, 14);
        }
        return cnpj;
    }
}
