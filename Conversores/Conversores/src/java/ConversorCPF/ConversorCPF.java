package ConversorCPF;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;


@FacesConverter(forClass = CpfMB.class)
public class ConversorCPF implements Converter {

    @Override
    // Método para converter o CPF formatado (011.778.929-10) para não formatado (01177892910)
    public Object getAsObject(FacesContext context, UIComponent component, String value) throws ConverterException {
        String cpf = value;
        if (value != null && !value.equals("")) {
            cpf = value.replaceAll("\\.", "").replaceAll("\\-", "");
        }
        return cpf;
    }

    @Override
    // Método para converter o CPF não formatado (01177892910) para formatado (011.778.929-10)
    public String getAsString(FacesContext context, UIComponent component, Object value) throws ConverterException {
        String cpf = value.toString();
        if (cpf != null && cpf.length() == 11) {
            cpf = cpf.substring(0, 3) + "." + cpf.substring(3, 6) + "." + cpf.substring(6, 9) + "-" + cpf.substring(9, 11);
        }

        return cpf;
     }
}
